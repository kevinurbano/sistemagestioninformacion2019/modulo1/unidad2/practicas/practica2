﻿USE Trabajadores;
    
-- 1.- Indicar el número de ciudades que hay en la tabla ciudades.
  SELECT 
    COUNT(*) numCiudades 
    FROM 
    ciudad c;

-- 2.- Indicar el nombre de las ciudades que tengan una población por encima de la población media.
  -- C1
  SELECT 
    AVG(c.población) 
    FROM 
      ciudad c;
  
  -- Final
  SELECT 
    DISTINCT c.nombre 
    FROM 
      ciudad c 
    WHERE 
      c.población > (SELECT 
                      AVG(c.población) 
                      FROM 
                        ciudad c);

-- 3.- Indicar el nombre de las ciudades que tengan una población por debajo de la población media.
  -- C1
  SELECT 
    AVG(c.población) 
    FROM 
      ciudad c;

  -- Final
  SELECT 
    DISTINCT c.nombre
    FROM 
      ciudad c
    WHERE
      c.población < (SELECT 
                      AVG(c.población) 
                      FROM 
                        ciudad c);
-- 4.- Indicar el nombre de la ciudad con la población máxima.
  -- C1
  SELECT 
    MAX(c.población) 
  FROM 
    ciudad c;

  -- Final
  SELECT 
    DISTINCT c.nombre
    FROM 
      ciudad c 
    WHERE 
      c.población=(SELECT 
                     MAX(c.población) 
                   FROM 
                     ciudad c);

-- 5.- Indicar el nombre de la ciudad con la población mínima.
  -- C1
  SELECT
    MIN(c.población) 
  FROM 
    ciudad c;

  -- Final
  SELECT 
    DISTINCT c.nombre
    FROM 
      ciudad c 
    WHERE 
      c.población=(SELECT 
                     MIN(c.población) 
                   FROM 
                     ciudad c);

-- 6.- Indicar el número de ciudades que tengan una población por encima de la población media.
  -- C1
  SELECT 
    AVG(c.población) 
  FROM 
    ciudad c;

  -- Final
  SELECT 
    COUNT(*) nCiudades
    FROM 
      ciudad c 
    WHERE 
      c.población=(SELECT 
                     MAX(c.población) 
                   FROM 
                     ciudad c);

-- 7.- Indicarme el número de personas que viven en cada ciudad.
  SELECT 
    p.ciudad,COUNT(*) nPersonas 
  FROM 
    persona p 
  GROUP BY 
    p.ciudad;
 
-- 8.- Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañias.
  SELECT 
    t.compañia, COUNT(*) nPersonas 
  FROM 
    trabaja t 
  GROUP BY 
    t.compañia;

-- 9.- Indicarme la compañía que mas trabajadores tiene.
  -- C1
  SELECT 
    t.compañia,COUNT(*) nPersonas 
  FROM 
    trabaja t 
  GROUP BY 
    t.compañia;

  -- C2
  SELECT MAX(nPersonas) maximo FROM 
    (
      SELECT 
        t.compañia,COUNT(*) nPersonas 
      FROM 
        trabaja t 
      GROUP BY 
        t.compañia
    ) c1;

  -- Final
  SELECT c1.compañia FROM
      (
        SELECT 
          t.compañia,COUNT(*) nPersonas 
        FROM 
          trabaja t 
        GROUP BY 
          t.compañia
      ) c1
    JOIN 
      (
        SELECT MAX(nPersonas) maximo FROM 
        (
          SELECT 
            t.compañia,COUNT(*) nPersonas 
          FROM 
            trabaja t 
          GROUP BY 
            t.compañia
        ) c1
      ) c2 
    ON c1.nPersonas=c2.maximo;

-- 10.- Indicarme el salario medio de cada una de las compañias.
  SELECT 
    t.compañia,AVG(t.salario) SalarioMedio 
  FROM  
    trabaja t 
  GROUP BY 
    t.compañia;

-- 11.- Listarme el nombre de las personas y la población de la ciudad donde viven.
  SELECT 
    DISTINCT p.nombre,c.población
  FROM 
    persona p 
  JOIN 
    ciudad c ON p.ciudad=c.nombre;

-- 12.- Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive.
  SELECT 
    p.nombre,p.calle,c.población
  FROM 
    persona p
  JOIN 
    ciudad c
  ON p.ciudad=c.nombre;

-- 13.- Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja.
  SELECT 
    p.nombre,p.ciudad,c.ciudad ciudadCompañia
  FROM 
    persona p
  JOIN 
    trabaja t
  ON p.nombre=t.persona
  JOIN
    compañia c
  ON t.compañia=c.nombre;

-- 14.- Realizar el algebra relacional y explicar la siguiente consulta.
  SELECT 
    p.nombre 
    FROM 
      persona p,trabaja t,compañia c
    WHERE
      p.nombre=t.persona
    AND
      t.compañia=c.nombre
    AND
      c.ciudad=p.ciudad
    ORDER BY
      p.nombre;


-- 15.- Listarme el nombre de la persona y el nombre de su supervisor.
  SELECT 
    s.persona,s.supervisor
  FROM 
    supervisa s;

-- 16.- Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos.
  SELECT 
    personas.nombre Personas, personas.ciudad CiudadesP, supervisor.nombre Supervisores, supervisor.ciudad CiudadesS
    FROM 
      supervisa s 
    JOIN 
      persona personas ON personas.nombre=s.persona
    JOIN
      persona supervisor ON supervisor.nombre=s.supervisor;

-- 17.- Indicarme el número de ciudades distintas que hay en la tabla compañía 
  SELECT 
    COUNT(DISTINCT c.ciudad) nCiudades
    FROM 
      compañia c;

-- 18.- Indicarme el número de ciudades distintas que hay en la tabla personas 
  SELECT 
    COUNT(DISTINCT p.ciudad) nCiudades
    FROM 
      persona p;

-- 19.- Indicarme el nombre de las personas que trabajan para FAGOR
  SELECT 
    DISTINCT t.persona
    FROM 
      trabaja t
    WHERE
      t.compañia='FAGOR';

-- 20.- Indicarme el nombre de las personas que no trabajan para FAGOR 
  SELECT 
    DISTINCT t.persona
    FROM 
      trabaja t
    WHERE
      t.compañia<>'FAGOR';

-- 21.- Indicarme el número de personas que trabajan  para INDRA
  SELECT 
    COUNT(DISTINCT t.persona) npersonas
    FROM 
      trabaja t
    WHERE
      t.compañia='INDRA';
-- 22.- Indicarme el nombre de las personas que trabajan para FAGOR o para INDRA
  SELECT 
    DISTINCT t.persona 
    FROM 
      trabaja t
    WHERE
      t.compañia IN ('FAGOR','INDRA');

-- 23.- Listar la población donde vive cada persona, sus salario, su nombre y la compañía para la que trabaja. 
--      Ordenar la salida por nombre de la persona y por salario de forma descendente.
  SELECT 
    DISTINCT c.población,t.salario,t.persona,t.compañia 
    FROM 
      trabaja t
    JOIN
      persona p ON t.persona=p.nombre
    JOIN
      ciudad c ON p.ciudad=c.nombre
    ORDER BY
      t.persona DESC,t.salario DESC;







































